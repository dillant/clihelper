install:
	cp clihelper.sh /usr/local/bin/clihelper
	cp jargon.sh /usr/local/bin/clijargon

uninstall:
	sudo rm /usr/local/bin/clihelper

update:
	sudo rm /usr/local/bin/clihelper
	sudo rm /usr/local/bin/clijargon
	cp clihelper.sh /usr/local/bin/clihelper
	cp jargon.sh /usr/local/bin/clijargon
