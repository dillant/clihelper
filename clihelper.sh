#!/usr/bin/env bash

export NEWT_COLORS='
       root=,red
'

function start() {
whiptail --title "CLIHELPER" --yesno "Hi it's $helpername do you need help?" 0 0 3>&1 1>&2 2>&3
if [[ $? == 1 ]]; then
    exit 1
fi
}

function helpmenu() {
    ANS=$(whiptail --title "CLIHELPER" --menu "What can I help you with?" 0 0 0 \
        "Managing Apps" "Learning the basics of the package manager" \
        "Basics" "Navigating the command line" \
        "Files" "Create files and folders" \
        3>&1 1>&2 2>&3
        )
    if [[ $ANS == "Managing Apps" ]]; then
        apps
    elif [[ $ANS == "Basics" ]]; then
        nav
    elif [[ $ANS == "Files" ]]; then
         files
    fi
}

function helpmenudeb() {
    ANS=$(whiptail --title "CLIHELPER" --menu "What can I help you with?" 0 0 0 \
        "Managing Apps" "Learning the basics of the package manager" \
        "Basics" "Navigating the command line" \
        "Files" "Create files and folders" \
        3>&1 1>&2 2>&3
        )
    if [[ $ANS == "Managing Apps" ]]; then
        appsdeb
    elif [[ $ANS == "Basics" ]]; then
        nav
    elif [[ $ANS == "Files" ]]; then
         files
    fi
}

function appsdeb() {
    help=$(whiptail --title "CLIHELPER" --menu "What do you need help with?" 0 0 0 \
        "Installing apps" "Package manager basics" \
        "Removing apps" "Package manager basics" \
        "Search for an app" "Package manager basics" \
        "Update your apps and system" "Package manager basics" \
        3>&1 1>&2 2>&3)
        if [[ $help == "Installing apps" ]]; then
           clear
           echo -e "\033[31myou can do this with the command (sudo apt install <package>)\033[0m"
           sleep 1
           echo "would you like me to execute this for you[n/y] (ps: its better to do it yourself)"
           read helpchoice
           if [[ $helpchoice == "y" ]]; then
               echo "What is the name of the package?"
               read app
               echo "sudo apt install $app"
               sudo apt install $app
               echo -e "\033[31mpackage installed!\033[0m"
               sleep 3
           else
               exit 1
           fi
           elif [[ $help == "Removing apps" ]]; then
               clear
               echo -e "\033[31mTo remove apps you can use the command (sudo apt install <package>)\033[0m"
               sleep 1
               echo "Would you like me to execute this for you?[n/y] (ps: its better to do it yourself)"
               read helpchoice
               if [[ $helpchoice == "y" ]]; then
                   clear
                   echo "What package would you like me to remove?"
                   read app
                   echo "sudo apt remove $app"
                   sudo apt remove $app
                   echo -e "\033[31mpackage removed!\033[0m"
                   sleep 3
               else
                   exit 1
               fi
        elif [[ $help == "Search for an app" ]]; then
               clear
               echo -e "\033[31mTo search for apps you can use the command (apt search <package>)\033[0m"
               sleep 1
               echo "Would you like me to execute this for you?[n/y] (ps: its better to do it yourself)"
               read helpchoice
               if [[ $helpchoice == "y" ]]; then
                   clear
                   echo "What package would you like me to search for?"
                   read app
                   echo "apt search $app"
                   apt search $app
                   exit 1
               else
                   exit 1
               fi

        elif [[ $help == "Update your apps and system" ]]; then
               clear
               echo -e "\033[31mTo update your apps you can use the command (sudo apt update && sudo apt upgrade)\033[0m"
               sleep 1
               echo "Would you like me to execute this for you?[n/y] (ps: its better to do it yourself)"
               read helpchoice
               if [[ $helpchoice == "y" ]]; then
                   clear
                   echo "sudo apt update && sudo apt upgrade"
                   sudo apt update && sudo apt upgrade
                   echo -e "\033[031System Updated!\033[0m"
               else
                   exit 1
               fi
        fi
}

function apps() {
    help=$(whiptail --title "CLIHELPER" --menu "What do you need help with?" 0 0 0 \
        "Installing apps" "Package manager basics" \
        "Removing apps" "Package manager basics" \
        "Search for an app" "Package manager basics" \
        "Update your apps and system" "Package manager basics" \
        3>&1 1>&2 2>&3)
        if [[ $help == "Installing apps" ]]; then
           clear
           echo -e "\033[31myou can do this with the command (sudo pacman -S <package>)\033[0m"
           sleep 1
           echo "would you like me to execute this for you[n/y] (ps: its better to do it yourself)"
           read helpchoice
           if [[ $helpchoice == "y" ]]; then
               echo "What is the name of the package?"
               read app
               echo "sudo pacman -S $app"
               sudo pacman -S $app
               echo -e "\033[31mpackage installed!\033[0m"
               sleep 3
           else
               exit 1
           fi
           elif [[ $help == "Removing apps" ]]; then
               clear
               echo -e "\033[31mTo remove apps you can use the command (sudo pacman -R <package>)\033[0m"
               sleep 1
               echo "Would you like me to execute this for you?[n/y] (ps: its better to do it yourself)"
               read helpchoice
               if [[ $helpchoice == "y" ]]; then
                   clear
                   echo "What package would you like me to remove?"
                   read app
                   echo "sudo pacman -R $app"
                   sudo pacman -R $app
                   echo -e "\033[31mpackage removed!\033[0m"
                   sleep 3
               else
                   exit 1
               fi
        elif [[ $help == "Search for an app" ]]; then
               clear
               echo -e "\033[31mTo search for apps you can use the command (sudo pacman -Ss <package>)\033[0m"
               sleep 1
               echo "Would you like me to execute this for you?[n/y] (ps: its better to do it yourself)"
               read helpchoice
               if [[ $helpchoice == "y" ]]; then
                   clear
                   echo "What package would you like me to search for?"
                   read app
                   echo "sudo pacman -Ss $app"
                   sudo pacman -Ss $app
                   exit 1
               else
                   exit 1
               fi

        elif [[ $help == "Update your apps and system" ]]; then
               clear
               echo -e "\033[31mTo update your apps you can use the command (sudo pacman -Syu)\033[0m"
               sleep 1
               echo "Would you like me to execute this for you?[n/y] (ps: its better to do it yourself)"
               read helpchoice
               if [[ $helpchoice == "y" ]]; then
                   clear
                   echo "sudo pacman -Syu"
                   sudo pacman -Syu
                   echo -e "\033[031System Updated!\033[0m"
               else
                   exit 1
               fi
        fi
}

function nav() {
    choice=$(whiptail  --title "CLIHELPER" --menu "What can I help you with?" 0 0 0 \
        "ls" "List all files and Folders in current Folder/Directory" \
        "cd" "entering and exiting folder (changing directorys)" \
        3>&1 1>&2 2>&3)
    if [[ $choice == "ls" ]]; then
        clear
        echo -e "\033[31mYou can do this with the command ((ls) or (ls -al) to see hidden folders)\033[0m"
        exit 1
    elif [[ $choice == "cd" ]]; then
        clear
        echo -e "\033[31mYou can do this with the command (cd <path to folder>)\033[0m"
        exit 1
    else
        exit 1
    fi

}

function files() {
    choice=$(whiptail --title "CLIHELPER" --menu "What would you like to do?" 0 0 0 \
        "mkdir" "creating a folder" \
        "rmdir" "deleting a folder" \
        "touch" "creating a file" \
        "rm" "deleting a file" \
        3>&1 1>&2 2>&3)

    if [[ $choice == "mkdir" ]]; then
        clear
        echo -e "\033[31mYou can do this with the command (mkdir <name of folder>)\033[0m"
        exit 1
    elif [[ $choice == "rmdir" ]]; then
        clear
        echo -e "\033[31mYou can do this with the command (rmdir <name of folder>)\033[0m"
        exit 1
    elif [[ $choice == "touch" ]]; then
        clear
        echo -e "\033[31mYou can do this with the command (touch <name of file>)\033[0m"
        exit 1
    elif [[ $choice == "rm" ]]; then
        clear
        echo -e "\033[31mYou can do this with the command (rm <name of file>)\033[0m"
        exit 1
    fi
}

function name() {
    name=$(whiptail --inputbox "What would you like my new name to be?" 0 0 --title "CLIHELPER" 3>&1 1>&2 2>&3)
    if [[ $? == 0 ]]; then
        echo $name > ~/.config/clihelper/name.txt
    fi
}

function help() {
echo "Usage: clihelper [flag]

no flag = just normal clihelper
--help = see this info
--name = change your clihelper's name
--jargon = learn some linux terminolgy and meanings"
}

case $# in
    1)
        case $1 in
            "--help" | "-h")
                help
                exit 1
                ;;
            "--name" | "-n")
                name
                exit 1
                ;;
            "--jargon")
                clijargon
                exit 1
                ;;
        esac
        ;;

    2)
        echo "Invalid: too many arguments"
        exit 1
        ;;


esac

x=0
while [[ $x != 1 ]]
 do

     if [ ! -d ~/.config/clihelper ]; then
         # if it doesn't then create the directory
         mkdir ~/.config/clihelper
     fi
     # checking if a name fo the cli helper has been named and stored
     if [ ! -f ~/.config/clihelper/name.txt ]; then
         # if not then create the file and store the name
         touch ~/.config/clihelper/name.txt
         name
     fi

     helpername=$(cat ~/.config/clihelper/name.txt)

     if [ ! -d /etc/pacman.d ]; then
     #     output main prompt
         start
     #     give help
         helpmenudeb
     fi

     clear
     # output the main prompt
     start
     # give help
     helpmenu

done
