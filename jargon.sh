#!/usr/bin/env bash

function start() {
    whiptail --title "CLIHELPER" --yesno "Hi its still $helpername, would you like to know some jargon?" 0 0 3>&1 1>&2 2>&3
}

function lesson() {
    ANS=$(whiptail --title "CLIHELPER" --menu "What lesson would you like to do?" 0 0 16 \
    "Lesson 1" "Learn the basics" \
    3>&1 1>&2 2>&3)
    if [[ $ANS == "Lesson 1" ]]; then
        lesson1
    fi
}

function lesson1() {
    whiptail --title "CLIHELPER" --msgbox "sudo = app that allows you run programs as the superuser from another user
~ = represents the home directory of the user
directory = its just like a folder
* = everything e.g. rm ~/* (this would delete everything in your home directory, so don't do this)" 0 0 3>&1 1>&2 2>&3

}
x=0
while [[ $x != 1 ]]
do
    helpername=$(cat ~/.config/clihelper/name.txt)
    clear
    start
    if [[ $? == "0" ]]; then
        clear
        lesson
    else
        exit 1
    fi
done
